import { About } from './about';
import { Josh } from './about';
import { Carousel } from './about';

export const AboutList: About[] = [
  { name: 'Experience', class: 'experience' },
  { name: 'Education', class: 'education' },
  { name: 'Skills', class: 'skills' },
  { name: 'General', class: 'general' }
];

export const JoshList: Josh[] = [
  {
    forename: 'Josh',
    surname: 'Thompson',
    area: 'Frontend Developer',
    gender: 'Male',
    dob: '12th July 1993',
    location: 'Bury St Edmunds, Suffolk, UK',
    cv: 'https://docs.google.com/document/d/1HQTLAF7rwyk5AqOw2Bjgsf3gQhspSm_MK3DKfA_N_uI/edit?usp=sharing'
  }
];

export const CarouselList: Carousel[] = [
  {name: 'Profile', src: 'profile'},
  {name: 'Football', src: 'football'},
  {name: 'Toughest', src: 'toughest'},
  {name: 'Gym', src: 'gym'},
  {name: 'Brunch', src: 'brunch'},
  {name: 'Landscape', src: 'landscape'},
  {name: 'Holiday', src: 'holiday'},
  {name: 'View', src: 'view'},
  {name: 'Dinner', src: 'dinner'}
];
