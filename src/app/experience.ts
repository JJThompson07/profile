export class Experience {
  level: string;
  details: {
    role: string;
    name: string;
    class: string;
    start: string;
    end: string;
    location: string;
    first: string;
    second: string;
    third: string;
  }[];
}
