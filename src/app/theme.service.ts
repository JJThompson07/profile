import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { Theme } from './theme';
import { ThemeList } from './themes-list';
import { ThemesComponent } from './themes/themes.component';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private themeSource = new BehaviorSubject<string>('plain');
  currentTheme = this.themeSource.asObservable();
  private nameSource = new BehaviorSubject<string>('Josh Thompson');
  currentName = this.nameSource.asObservable();
  private titleSource = new BehaviorSubject<string>('Frontend Developer');
  currentTitle = this.titleSource.asObservable();
  private dobSource = new BehaviorSubject<string>('12 / 07 / 1993');
  currentDOB = this.dobSource.asObservable();

  changeTheme(themeName: string) {
    this.themeSource.next(themeName);
  }

  changeName(profileName: string) {
    this.nameSource.next(profileName);
  }

  changeTitle(profileTitle: string) {
    this.titleSource.next(profileTitle);
  }

  changeDOB(profileDOB: string) {
    this.dobSource.next(profileDOB);
  }

  getThemes() {
    return ThemeList;
  }

  constructor() { }
}
