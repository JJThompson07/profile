import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Contact } from './contact';
import { ContactList } from './contact-list';
import { ContactComponent } from './contact/contact.component';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  getContact() {
    return ContactList;
  }

  constructor() { }
}