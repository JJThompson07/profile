export class About {
  name: string;
  class: string;
}

export class Josh {
  forename: string;
  surname: string;
  area: string;
  gender: string;
  dob: string;
  location: string;
  cv: string;
}

export class Carousel {
  name: string;
  src: string;
}
