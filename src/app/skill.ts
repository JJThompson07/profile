export class Skill {
  area: string;
  class: string;
  items: {
    name: string;
    areaclass: string;
    confidence: number;
    experience: string;
  }[];
}
