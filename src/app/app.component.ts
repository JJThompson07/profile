import { Component, OnInit } from '@angular/core';
import { Theme } from './theme';
import { ThemeList } from './themes-list';
import { ThemesComponent } from './themes/themes.component';
import { ThemeService } from './theme.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Josh Thompson';
  subtitle = 'Frontend Developer';
  themeName: string;
  themeClass = 'plain';

  constructor(private data: ThemeService) { }

  ngOnInit() {
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
  }

  receiveTheme($event) {
    this.themeClass = $event;
  }
}
