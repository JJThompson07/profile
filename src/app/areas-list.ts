import { Area } from './area';

export const AreasList: Area[] = [
  { name: 'HTML', class: 'html' },
  { name: 'CSS', class: 'css' },
  { name: 'SASS', class: 'sass' },
  { name: 'LESS', class: 'less' },
  { name: 'BEM', class: 'bem' },
  { name: 'Vanilla Js', class: 'vanilla-js' },
  { name: 'JQuery', class: 'jquery' },
  { name: 'Angular 7', class: 'angular-7' },
  { name: 'Ionic', class: 'ionic' },
  { name: 'NPM', class: 'npm' },
  { name: 'Laravel-mix', class: 'laravel-mix' },
  { name: 'Grunt', class: 'grunt' },
  { name: 'Drupal 7', class: 'drupal-7' },
  { name: 'Drupal 8', class: 'drupal-8' },
  { name: 'MYSQL', class: 'mysql' },
  { name: 'Jenkins', class: 'jenkins' },
  { name: 'UI/UX', class: 'ui-ux' },
  { name: 'Taiga', class: 'taiga' },
  { name: 'Trello', class: 'trello' },
  { name: 'Jira', class: 'jira' },
  { name: 'Browserstack', class: 'browserstack' },
  { name: 'AXE', class: 'axe' }
];
