import { Component, OnInit } from '@angular/core';
import { Nav } from '../nav';
import { NavList } from '../nav-list';
import { ThemeService } from '../theme.service';
import { ThemesComponent } from '../themes/themes.component';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  navs = NavList;
  themeName: string;
  profileName: string;
  profileTitle: string;


  constructor(private data: ThemeService) { }

  ngOnInit() {
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
    this.data.currentName.subscribe(profileName => this.profileName = profileName);
    this.data.currentTitle.subscribe(profileTitle => this.profileTitle = profileTitle);
  }
}
