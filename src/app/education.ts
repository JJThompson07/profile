export class Education {
  name: string;
  location: string;
  level: string;
  start: number;
  end: number;
  subjects: {
    name: string;
    grade: string;
  }[];
}
