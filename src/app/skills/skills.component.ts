import { Component, OnInit } from '@angular/core';
import { Skill } from '../skill';
import { SkillsList } from '../skills-list';
import { Area } from '../area';
import { AreasList } from '../areas-list';
import { SkillsService } from '../skills.service';
import { AreasService } from '../areas.service';
import { ThemeService } from '../theme.service';
import { ThemesComponent } from '../themes/themes.component';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})

export class SkillsComponent implements OnInit {

  areas = [];
  skills = [];
  isCollapsed = true;
  themeName: string;
  skillClass: any;
  themeClass = 'plain';

  constructor(private skillsData: SkillsService, private areasData: AreasService, private data: ThemeService) { }

  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  ngOnInit() {
    this.areas = this.areasData.getAreas();
    this.skills = this.skillsData.getSkills();
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
  }

  receiveTheme($event) {
    this.themeClass = $event;
  }

  skillClick(style: Skill) {
    this.skillClass = style.class;
    console.log(this.skillClass);
  }

  skillReset(style: Skill) {
    this.skillClass = '';
    console.log(this.skillClass);
  }
}
