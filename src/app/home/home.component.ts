import { Component, OnInit } from '@angular/core';
import { Theme } from '../theme';
import { ThemeList } from '../themes-list';
import { ThemeService } from '../theme.service';
import { ThemesComponent } from '../themes/themes.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  themes = [];
  themeName: string;
  profileName: string;
  profileTitle: string;
  profileDOB: string;
  themeClass = 'plain';



  constructor(private data: ThemeService) { }

  ngOnInit() {
    this.themes = this.data.getThemes();
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
    this.data.currentName.subscribe(profileName => this.profileName = profileName);
    this.data.currentTitle.subscribe(profileTitle => this.profileTitle = profileTitle);
    this.data.currentDOB.subscribe(profileDOB => this.profileDOB = profileDOB);
  }

  themeClick(style: Theme) {
    this.themeClass = style.class;
    this.data.changeTheme(this.themeClass);
  }
}
