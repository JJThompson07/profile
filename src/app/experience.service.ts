import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { Experience } from './experience';
import { ExperienceList } from './experience-list';
import { AboutComponent } from './about/about.component';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  constructor() { }

   getExperiences() {
    return ExperienceList;
  }
}
