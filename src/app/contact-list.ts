import { Contact } from './contact';

export const ContactList: Contact[] = [
  { type: 'Contact me',
    details: [
      { name: 'Phone',
        class: 'phone',
        text: 'Contact me via mobile',
        value: 'Call me',
        href: 'tel:+447464747581',
        src: '/assets/img/icons/social/phone--icon.png'
      },
      { name: 'E-mail',
        class: 'email',
        text: 'Contact me via E-mail',
        value: 'Email me',
        href: 'mailto:joshuajthompson@hotmail.co.uk',
        src: '/assets/img/icons/social/email--icon.png'
       },
      { name: 'LinkedIn',
        class: 'linkedin',
        text: 'LinkedIn profile',
        value: 'Message me',
        href: 'https://www.linkedin.com/in/jjthompson07/',
        src: '/assets/img/icons/social/linkedin--icon.png'
      }
    ]
  },
  { type: 'Accounts',
    details: [
      { name: 'Github',
        class: 'github',
        text: 'Personal Github account',
        value: 'Go to',
        href: 'https://github.com/JJThompson07',
        src: '/assets/img/icons/social/github--icon.png'
      },
      { name: 'Gitlab',
        class: 'gitlab',
        text: 'Personal Gitlab account',
        value: 'Go to',
        href: 'https://gitlab.com/JJThompson07',
        src: '/assets/img/icons/social/gitlab--icon.png'
      },
      { name: 'Instagram',
        class: 'instagram',
        text: 'Personal Instagram account',
        value: 'Go to',
        href: 'https://www.instagram.com/josh.thompson.07/',
        src: '/assets/img/icons/social/instagram--icon.png'
      }
    ]
  }
  
];
