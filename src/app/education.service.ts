import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { Education } from './education';
import { EducationList } from './education-list';
import { AboutComponent } from './about/about.component';

@Injectable({
  providedIn: 'root'
})
export class EducationService {

  constructor() { }

  getEducations() {
    return EducationList;
  }
}
