import { Education } from './education';

export const EducationList: Education[] = [
  { name: 'The University of Sheffield',
    location: 'Sheffield',
    level: 'Degree',
    start: 20011,
    end: 2015,
    subjects: [
      { name: 'Chemical Engineeering', grade: 'BENG'}
    ]
  },
  { name: 'The Ecclesbourne Schoool',
    location: 'Duffield, Derbyshire',
    level: 'Alevel',
    start: 2009,
    end: 2011,
    subjects: [
      { name: 'Maths', grade: 'A'},
      { name: 'Chemistry', grade: 'B'},
      { name: 'Biology', grade: 'B'}
    ]
  },
  { name: 'The Ecclesbourne Schoool',
    location: 'Duffield, Derbyshire',
    level: 'GCSE',
    start: 2003,
    end: 2009,
    subjects: [
      { name: 'Maths', grade: 'A*'},
      { name: 'Chemistry', grade: 'A*'},
      { name: 'Biology', grade: 'A*'},
      { name: 'Physical Education', grade: 'A*'},
      { name: 'Physics', grade: 'A'},
      { name: 'Geography', grade: 'A'},
      { name: 'Spanish', grade: 'A'},
      { name: 'Business', grade: 'A'},
      { name: 'Business & Communications', grade: 'B'},
      { name: 'English', grade: 'B'},
      { name: 'English Literature', grade: 'C'}
    ]
  }
];
