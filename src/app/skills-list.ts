import { Skill } from './skill';

export const SkillsList: Skill[] = [
  { area: 'HTML',
    class: 'html',
    items: [
      { name: 'HTML5', areaclass: 'html', confidence: 10, experience: 'Commercial'},
      { name: 'Twig', areaclass: 'html', confidence: 10, experience: 'Commercial'},
      { name: 'Pre-processes', areaclass: 'php', confidence: 7, experience: 'Commercial'},
      { name: 'hook Atlers', areaclass: 'php', confidence: 7, experience: 'Commercial'},
      { name: 'module', areaclass: 'php', confidence: 7, experience: 'Commercial'},
      { name: 'Devel', areaclass: 'drupal', confidence: 8, experience: 'Commercial'}
    ]
  },
  { area: 'CSS',
    class: 'css',
    items: [
      { name: 'CSS3', areaclass: 'css', confidence: 10, experience: 'Commercial'},
      { name: 'SCSS', areaclass: 'css', confidence: 10, experience: 'Commercial'},
      { name: 'Mixins', areaclass: 'css', confidence: 10, experience: 'Commercial'},
      { name: 'LESS', areaclass: 'css', confidence: 10, experience: 'Commercial'},
      { name: 'Flex', areaclass: 'css layout', confidence: 10, experience: 'Commercial'},
      { name: 'Grid', areaclass: 'css layout', confidence: 8, experience: 'Commercial'},
      { name: 'BEM', areaclass: 'css layout', confidence: 10, experience: 'Commercial'},
      { name: 'Bootstrap', areaclass: 'css framework', confidence: 10, experience: 'Commercial'},
    ]
  },
  { area: 'Javascript',
    class: 'javascript',
    items: [
      { name: 'Vanilla Js', areaclass: 'javascript', confidence: 9, experience: 'Commercial'},
      { name: 'jQuery', areaclass: 'javascript', confidence: 9, experience: 'Commercial'},
      { name: 'Grunt', areaclass: 'js taskrunners', confidence: 9, experience: 'Commercial'},
      { name: 'SASS', areaclass: 'js taskrunners', confidence: 9, experience: 'Commercial'},
      { name: 'Laravel-mix', areaclass: 'js taskrunners', confidence: 8, experience: 'Independent'},
      { name: 'Angular 7', areaclass: 'js frameworks', confidence: 8, experience: 'Independent'},
      { name: 'Ionic', areaclass: 'js frameworks', confidence: 7, experience: 'Independent'},
      { name: 'NPM', areaclass: 'node', confidence: 7, experience: 'Commercial'}
    ]
  },
  { area: 'CMS',
    class: 'cms',
    items: [
      { name: 'Drupal 7', areaclass: 'cms', confidence: 8, experience: 'Commercial'},
      { name: 'Drupal 8', areaclass: 'cms', confidence: 9, experience: 'Commercial'}
    ]
  },
  { area: 'Server',
    class: 'server',
    items: [
      { name: 'NGINX', areaclass: 'server', confidence: 8, experience: 'Commercial'},
      { name: 'MYSQL', areaclass: 'server', confidence: 9, experience: 'Commercial'},
      { name: 'Drush', areaclass: 'drupal', confidence: 8, experience: 'Commercial'},
      { name: 'Jenkins', areaclass: 'server', confidence: 8, experience: 'Commercial'}
    ]
  },
  { area: 'Accessibility',
    class: 'accessibility',
    items: [
      { name: 'UI', areaclass: 'design', confidence: 10, experience: 'Commercial'},
      { name: 'UX', areaclass: 'design', confidence: 10, experience: 'Commercial'},
      { name: 'AXE', areaclass: 'accessibility', confidence: 7, experience: 'Commercial'},
      { name: 'JAWS', areaclass: 'accessibility', confidence: 7, experience: 'Commercial'},
      { name: 'Browserstack', areaclass: 'accessibility', confidence: 9, experience: 'Commercial'}
    ]
  },
  { area: 'Other',
    class: 'other',
    items: [
      { name: 'Jira', areaclass: 'project management', confidence: 10, experience: 'Commercial'},
      { name: 'Trello', areaclass: 'project management', confidence: 8, experience: 'Commercial'},
      { name: 'Taiga', areaclass: 'project management', confidence: 8, experience: 'Commercial'},
      { name: 'Excel', areaclass: 'other', confidence: 10, experience: 'Commercial'},
      { name: 'Google Sheets', areaclass: 'other', confidence: 10, experience: 'Commercial'},
      { name: 'Word', areaclass: 'other', confidence: 10, experience: 'Commercial'}
    ]
  }
];
