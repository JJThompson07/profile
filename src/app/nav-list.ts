import { Nav } from './nav';

export const NavList: Nav[] = [
  { route: '', name: 'Home' },
  { route: 'about', name: 'About' },
  { route: 'contact', name: 'Contact' },
  { route: 'misc', name: 'Misc' }
];
