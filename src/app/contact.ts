export class Contact {
  type: string;
  details: {
    name: string;
    class: string;
    text: string;
    value: string;
    href: string;
    src: string;
  }[];
}
