import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { About} from './about';
import { Josh } from './about';
import { Carousel } from './about';
import { AboutList} from './about-list';
import { JoshList } from './about-list';
import { CarouselList } from './about-list';
import { AboutComponent } from './about/about.component';

@Injectable({
  providedIn: 'root'
})
export class AboutService {

  getAbout() {
    return AboutList;
  }

  getJosh() {
    return JoshList;
  }

  getCarousel() {
    return CarouselList;
  }

  constructor() { }

}
