import { Theme } from './theme';

export const ThemeList: Theme[] = [
  { name: 'Midnight', class: 'midnight'},
  { name: 'Sunset', class: 'sunset'},
  { name: 'Chic', class: 'chic'},
  { name: 'Forest', class: 'forest'},
  { name: 'Plain', class: 'plain'}
];
