import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Theme } from '../theme';
import { ThemeList } from '../themes-list';
import { ThemeService } from '../theme.service';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.scss', '../themes.scss']
})
export class ThemesComponent implements OnInit {

  themes = [];
  isCollapsed = true;
  themeName: string;
  profileName: string;
  themeClass = 'plain';


  constructor(private data: ThemeService) { }

  ngOnInit() {
    this.themes = this.data.getThemes();
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
    this.data.currentName.subscribe(profileName => this.profileName = profileName);
  }

  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  themeClick(style: Theme) {
    this.themeClass = style.class;
    this.data.changeTheme(this.themeClass);
  }

}
