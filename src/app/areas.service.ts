import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Area } from './area';
import { AreasList } from './areas-list';
import { SkillsComponent } from './skills/skills.component';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  getAreas() {
    return AreasList;
  }

  constructor() { }
}
