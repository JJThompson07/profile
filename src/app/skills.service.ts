import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Skill } from './skill';
import { SkillsList } from './skills-list';
import { SkillsComponent } from './skills/skills.component';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  getSkills() {
    return SkillsList;
  }

  constructor() { }
}
