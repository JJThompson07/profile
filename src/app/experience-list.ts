import { Experience } from './experience';

export const ExperienceList: Experience[] = [
  { level: 'Independent',
    details: [
      { role: 'Frontend Developer',
        name: 'SAP Construction',
        class: 'sap-construction',
        start: 'April 2019',
        end: 'Present',
        location: 'Bury St Edmunds, UK',
        first: 'I am currently  in the process of delivering a drupal based website for SAP Construction. ',
        second: 'Integrating previous commercial experience, from my time at Zoocha Ltd, as well as independent experience to develop a clean,  responsive, mobile-first site.',
        third: ''
      }
    ]
  },
  { level: 'Commercial',
    details: [
      { role: 'Frontend Developer',
        name: 'Zoocha Ltd',
        class: 'zoocha-ltd',
        start: 'January 2018',
        end: 'April 2019',
        location: 'Hertford, UK',
        first: 'Developed well-structured front end, incorporated with the BEM method allowing for reusable code as well as increased development speed and debugging.',
        second: 'SASS compilers along with coding standards were used to increase site performance and page load speeds. Regular use of Browserstack, axe and JAWS software allowed for an detailed focus on the accessibility of sites across browsers.Day to day work requires the ability to strategically plan and build a variety of complex web pages, components and elements.',
        third: ''
      },
      { role: 'Graduate Developer',
        name: 'Zoocha Ltd',
        class: 'zoocha-ltd',
        start: 'October 2016',
        end: 'January 2018',
        location: 'Hertford, UK',
        first: 'The start of my commercial experience as a Frontend Developer. As a graduate developer I was provided with the perfect opportunity for progression of my understanding of code and project delivery aswell as developing a better perspective on how to tackle tasks and issues from the clients. ',
        second: 'I was introduced to the CMS of both Drupal 7 and Drupal 8. I quicky adapted to the working environment and continuously aspired to improve my coding standard and product delivery.',
        third: ''
      },
      { role: 'Customer Facing Coordinator',
        name: 'Pattonair Ltd',
        class: 'pattonair-ltd',
        start: 'September 2015',
        end: 'April 2016',
        location: 'Derby, UK',
        first: 'Extraction, manipulation and analysis of data from customer or internal databases and Excel. I was responsible for the measurement of KPI’s and all RMA forms for UK based customers. Regular communication with my Clients was crucial to ensure correct shipments were made to meet the forecasted and demanded orders.',
        second: 'In addition to my role, I was asked to produce improved weekly financial reporting, for the Finance Director and Senior Leadership Team, by extracting information across many different databases, manipulating the data into a presentable format including pivot tables and graphs comparing key information within the company.',
        third: ''
      }
    ]
  }
];
