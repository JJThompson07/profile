import { Component, OnInit, HostBinding } from '@angular/core';
import { About } from '../about';
import { AboutList } from '../about-list';
import { AboutService } from '../about.service';
import { Education } from '../education';
import { EducationList } from '../education-list';
import { EducationService } from '../education.service';
import { ExperienceService } from '../experience.service';
import { ThemeService } from '../theme.service';
import { ThemesComponent } from '../themes/themes.component';
import { trigger, transition, animate, style } from '@angular/animations';
import '../../jquery';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})

export class AboutComponent implements OnInit {

  educations = [];
  experiences = [];
  abouts = [];
  joshs = [];
  carousels = [];
  themeName: string;
  // sectionName = 'skills';
  sectionName = 'general';

  carouselOptions = {
    margin: 15,
    dots: false,
    loop: true,
    lazyLoad: true,
    autoplay: true,
    smartSpeed: 500,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    nav: false,
    items: 1
  };

  constructor(private eds: EducationService, private exps: ExperienceService, private data: ThemeService, private abs: AboutService) { }


  ngOnInit() {
    this.abouts = this.abs.getAbout();
    this.joshs = this.abs.getJosh();
    this.carousels = this.abs.getCarousel();
    this.educations = this.eds.getEducations();
    this.experiences = this.exps.getExperiences();
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
  }

  aboutToggle(section: About) {
    this.sectionName = section.class;
    console.log(this.sectionName);
  }
}
