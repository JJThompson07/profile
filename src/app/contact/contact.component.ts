import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../theme.service';
import { ContactService } from '../contact.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contacts = [];
  themeName: string;

  constructor(private contactData: ContactService, private data: ThemeService) { }

  ngOnInit() {
    this.contacts = this.contactData.getContact();
    this.data.currentTheme.subscribe(themeName => this.themeName = themeName);
  }

}
